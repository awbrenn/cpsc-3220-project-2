#include "thread_queue.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argv, char *argc[]) {
    thread_queue *t_queue;
    thread_node *new_node1;
    thread_node *new_node2;
    thread_node *new_node3;
    thread_node *dequeued_node;
    int tid = 1;

    t_queue = createQueue();

    new_node1 = (thread_node *) malloc(sizeof(thread_node));
    new_node1->tid = tid++;
    enqueue(t_queue, new_node1);

    new_node2 = (thread_node *) malloc(sizeof(thread_node));
    new_node2->tid = tid++;
    enqueue(t_queue, new_node2);

    new_node3 = (thread_node *) malloc(sizeof(thread_node));
    new_node3->tid = tid++;
    enqueue(t_queue, new_node3);

    while(t_queue->head != NULL) {
        dequeued_node = dequeue(t_queue);
        printf("\nthread id is %d\n", dequeued_node->tid);
        free(dequeued_node);
    }

    if(isEmpty(t_queue)) {
        printf("\nempty queue\n");
    }

    thread_node *found_node;

    new_node1 = (thread_node *) malloc(sizeof(thread_node));
    new_node1->tid = tid++;
    enqueue(t_queue, new_node1);

    new_node2 = (thread_node *) malloc(sizeof(thread_node));
    new_node2->tid = tid++;
    enqueue(t_queue, new_node2);

    new_node3 = (thread_node *) malloc(sizeof(thread_node));
    new_node3->tid = tid++;
    enqueue(t_queue, new_node3);

    found_node = findThreadNode(t_queue, 6);

    if (found_node == NULL) {
        printf("Was not found.");
    }
    else {
        printf("\nthread id of found thread is %d\n", found_node->tid);
    }

    while(t_queue->head != NULL) {
        dequeued_node = dequeue(t_queue);
        printf("\nthread id is %d\n", dequeued_node->tid);
        free(dequeued_node);
    }

    printf("\nChecking enqueueAtHead()\n");

    new_node1 = (thread_node *) malloc(sizeof(thread_node));
    new_node1->tid = tid++;
    enqueue(t_queue, new_node1);

    new_node2 = (thread_node *) malloc(sizeof(thread_node));
    new_node2->tid = tid++;
    enqueue(t_queue, new_node2);

    new_node3 = (thread_node *) malloc(sizeof(thread_node));
    new_node3->tid = tid++;

    enqueueAtHead(t_queue, new_node3);
    while(t_queue->head != NULL) {
        dequeued_node = dequeue(t_queue);
        printf("\nthread id is %d\n", dequeued_node->tid);
        free(dequeued_node);
    }

    free(t_queue);
}