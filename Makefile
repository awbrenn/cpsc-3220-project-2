CC = clang
CCFLAGS = -Wall

all: threadlib

threadlib:
	-rm *.o *.a *.out
	$(CC) $(CCFLAGS) -c mythreads.c thread_queue.c
	ar -cvq libmythreads.a mythreads.o thread_queue.o
