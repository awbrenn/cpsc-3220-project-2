#include <stdio.h>
#include <stdlib.h>
#include "mythreads.h"

void *t1 (void *arg) {
    threadLock(0);
    int param = *((int*)arg);

    int *result = malloc(sizeof(int));
    param *= 5;
    *result = param;

    threadYield();
//    threadWait(0, 1);

    threadUnlock(0);

    return  result;
}

void *t2 (void *arg) {
    threadLock(0);

    int param = *((int*)arg);
    int *result = malloc(sizeof(int));
    param += 5;
    *result = param;

    threadSignal(0, 1);

    threadUnlock(0);

    return result;
}

int main(void) {
    int id1, id2;

    int p1 = 20;

    int *result1, *result2;

    threadInit();

    id1 = threadCreate(t1,(void*)&p1);
    printf("created thread 1.\n");

    id2 = threadCreate(t2,(void*)&p1);
    printf("created thread 2.\n");

    threadJoin(id1, (void*)&result1);
    printf("joined #1 --> %d.\n",*result1);

    threadJoin(id2, (void*)&result2);
    printf("joined #2 --> %d.\n",*result2);
}