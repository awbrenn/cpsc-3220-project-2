#include "mythreads.h"

typedef struct thread_node {
    int tid;
    ucontext_t *context;
    thFuncPtr funcPtr;
    void *argPtr;
    void *result;
    struct thread_node *next;
} thread_node;

typedef struct thread_queue {
    struct thread_node *head, *tail;
} thread_queue;


int isEmpty(thread_queue *t_queue);
void enqueueAtHead(thread_queue *, thread_node *);
thread_node *findThreadNode(thread_queue *, int);
thread_queue *createQueue(void);
void enqueue(thread_queue *, thread_node *);
thread_node *dequeue(thread_queue *t_queue);
