#include "thread_queue.h"
#include <stdio.h>
#include <stdlib.h>

thread_queue *ready_queue;
thread_queue *done_queue;
thread_queue *waiting_queues[NUM_LOCKS][CONDITIONS_PER_LOCK];
int THREAD_LOCKS[NUM_LOCKS]; // 0 is locked; 1 is locked
int THREAD_COUNT = 0;
int interruptsAreDisabled;

void threadInit() {
    interruptsAreDisabled = 1;

    // initialize ready and done queues
    ready_queue = createQueue();
    done_queue = createQueue();

    // initialize waiting queues
    int i, j;
    for(i = 0; i < NUM_LOCKS; i++) {
        for(j = 0; j < CONDITIONS_PER_LOCK; j++) {
            waiting_queues[i][j] = createQueue();
        }
    }

    // initialize locks
    for(i = 0; i < NUM_LOCKS; i++) {
        THREAD_LOCKS[i] = 0;
    }

    // capture information about the main thread
    thread_node *t_node;
    t_node = malloc(sizeof(thread_node));
    t_node->context = malloc(sizeof(ucontext_t));

    getcontext(t_node->context);
    t_node->tid = THREAD_COUNT++;

    enqueue(ready_queue, t_node);

    interruptsAreDisabled = 0;
}

void wrapper(thread_node *t_node) {
    /*
        - calls function i.e. f(null)
        - threadExit();
    */
    thFuncPtr func = t_node->funcPtr;

    void *result;
    interruptsAreDisabled = 0;
    result = func(t_node->argPtr);
    interruptsAreDisabled = 1;
    threadExit(result);
}

int threadCreate(thFuncPtr funcPtr, void *argPtr) {
    interruptsAreDisabled = 1;
    /*
        - make a new node
        - put new node at the front of the ready queue
        - put previous running thread at the end of the ready queue
        - make a new context
            - makecontext(context c, void *(void) wrapper, 2, x, y[different from user function])
        - swap context to the new thread
     */

    // setup thread_node
    thread_node *running_node;
    thread_node *new_node;
    new_node = malloc(sizeof(thread_node));
    new_node->context = malloc(sizeof(ucontext_t));

    getcontext(new_node->context);
    new_node->context->uc_stack.ss_size = STACK_SIZE;
    new_node->context->uc_stack.ss_sp = (void *) malloc(STACK_SIZE);
    new_node->context->uc_stack.ss_flags = 0;
    new_node->tid = THREAD_COUNT++;
    new_node->funcPtr = funcPtr;
    new_node->argPtr = argPtr;

    // move currently running thread to the back of the queue
    running_node = dequeue(ready_queue);
    enqueue(ready_queue, running_node);

    // put the new thread at the front of the queue
    enqueueAtHead(ready_queue, new_node);

    // make a new context for the new_node
    makecontext(new_node->context, (void (*)(void))wrapper, 1, new_node);

    // swap contexts
    swapcontext(running_node->context, new_node->context);

    interruptsAreDisabled = 0;
    return new_node->tid;
}

void threadYield() {
    /*
        - move node to end
        - swapcontext
     */
    interruptsAreDisabled = 1;
    thread_node *t_node;

    t_node = dequeue(ready_queue);
    enqueue(ready_queue, t_node);

    swapcontext(t_node->context, ready_queue->head->context);
    interruptsAreDisabled = 0;
}

void threadJoin(int thread_id, void **result) {
    /*
        ** magic
        -
        - wait until the thread is finished
        - retrieve the result

     */
    interruptsAreDisabled = 1;

    int thread_done_flag = 0;
    thread_node * done_node;

    while (thread_done_flag == 0) {
        /* - run through done queue - queue_array[1]
           - check the tid of the done threads
           - if thread not done, move this thread to end of ready queue
           - swapcontext to next runnable thread
           - if thread is done set thread_done to 1
         */

        done_node = findThreadNode(done_queue, thread_id);
        if (done_node != NULL) {
            thread_done_flag = 1;
            *result = done_node->result;
        }
        else {
            threadYield();
        }
    }
    interruptsAreDisabled = 0;
}

void threadExit(void *result) {
    interruptsAreDisabled = 1;
    thread_node *t_node;

    // get currently running thread_node
    t_node = dequeue(ready_queue);

    // set the result of thread_node
    t_node->result = result;

    // free the stack of thread_node
    free(t_node->context->uc_stack.ss_sp);
    t_node->context->uc_stack.ss_size = 0;

    // put exited thread on the done queue queue_array[1]
    enqueue(done_queue, t_node);

    if (ready_queue->head != NULL) {
        swapcontext(t_node->context, ready_queue->head->context);
    }
    interruptsAreDisabled = 0;
}

void threadLock(int lockNum) {
    interruptsAreDisabled = 1;

    int got_lock = 0;

    while(got_lock == 0) {
        if (THREAD_LOCKS[lockNum] == 0) {
            got_lock = 1;
            THREAD_LOCKS[lockNum] = 1;
        }
        else {
            threadYield();
        }
    }
    interruptsAreDisabled = 0;
}

void threadUnlock(int lockNum) {
    interruptsAreDisabled = 1;
    THREAD_LOCKS[lockNum] = 0;
    interruptsAreDisabled = 0;
}

void threadWait(int lockNum, int conditionNum) {
    interruptsAreDisabled = 1;

    thread_node *wait_node;
    wait_node = dequeue(ready_queue);

    threadUnlock(lockNum);
    enqueue(waiting_queues[lockNum][conditionNum], wait_node);
    swapcontext(wait_node->context, ready_queue->head->context);
    threadLock(lockNum);

    interruptsAreDisabled = 0;
}

void threadSignal(int lockNum, int conditionNum) {
    interruptsAreDisabled = 1;

    thread_node *signaled_node;

    signaled_node = dequeue(waiting_queues[lockNum][conditionNum]);
    if (signaled_node != NULL) {
        enqueue(ready_queue, signaled_node);
    }

    interruptsAreDisabled = 0;
}