#include <stdio.h>
#include <stdlib.h>
#include "thread_queue.h"

void handleError(char *error_message, int kill) {
    fprintf(stderr, "%s", error_message);
    if(kill == 1) {
        exit(1); // kill process if kill is set to one
    }
}

int isEmpty(thread_queue *t_queue) {
    int empty = 0;
    if (t_queue->head == NULL) {
        empty = 1;
    }
    return empty;
}

/*
    Enqueue at the head of the linked list rather than the tail
 */
void enqueueAtHead(thread_queue *t_queue, thread_node *new_node) {
    thread_node *temp_node;

    if (t_queue->head == NULL) {
        t_queue->head = t_queue->tail = new_node;
        new_node->next = NULL;
    }
    else {
        temp_node = t_queue->head;
        t_queue->head = new_node;
        new_node->next = temp_node;
    }
}

/*
    Searches queue for thread_node with specified tid
    Returns NULL if the node was not found
    Returns a pointer to the node if it was found
 */
thread_node *findThreadNode(thread_queue *t_queue, int tid) {
    thread_node *t_node;
    int found = 0;

    t_node = t_queue->head;
    while (found != 1 && t_node != NULL) {
        if (t_node->tid == tid) {
            found = 1;
        }
        else {
            t_node = t_node->next;
        }
    }

    return t_node;
}

thread_queue *createQueue() {
    thread_queue *t_queue;

    t_queue = (thread_queue *) malloc(sizeof(thread_queue));

    if (t_queue == NULL) {
        handleError("Insufficiant memory to create queue", 1);
    }

    t_queue->head = NULL;
    t_queue->tail = NULL;

    return t_queue;
}

void enqueue(thread_queue *t_queue, thread_node *new_node) {
    if (new_node == NULL) {
        handleError("Insufficiant memory to create new node", 1);
    }

    if (t_queue->head == NULL) {
        t_queue->head = t_queue->tail = new_node;
        new_node->next = NULL;
    }
    else {
        t_queue->tail->next = new_node;
        t_queue->tail = new_node;
        new_node->next = NULL;
    }
}

thread_node *dequeue(thread_queue *t_queue) {
    thread_node *dequeued_node;

    if(t_queue->head == NULL) {
        return NULL;
    }
    else if(t_queue->head == t_queue->tail) {
        dequeued_node = t_queue->head;
        t_queue->head = t_queue->tail = NULL;
    }
    else {
        dequeued_node = t_queue->head;
        t_queue->head = dequeued_node->next;
        dequeued_node->next = NULL;
    }

    return dequeued_node;
}